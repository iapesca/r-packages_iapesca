% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/Set_NetThresholds.R
\name{Set_NetThresholds}
\alias{Set_NetThresholds}
\title{Set_NetThresholds}
\usage{
Set_NetThresholds(ls.Nets)
}
\arguments{
\item{ls.Nets}{object x$Nets, output of Create_NetsByBoat function, a list of spatial linestring objects of class sf, each of them being the output of Create_Nets function applied to the fishing trips
or Nets, a spatial linestring object of class sf, output of Create_Nets}
}
\value{
comb.test, a data-frame with 1 line and 2 columns named "qt" and "iter"
}
\description{
\code{Set_NetThresholds} seeks for the best combination of arguments qt and iter to be used in Calc_NetsThresholds.
Tests iteratively designs of values for qt and iter and selects their combination that maximizes the decay of thresholds.
}
\examples{

run.example <- FALSE

if(run.example){

  require(mapview)
  data(positions)

  pos <- df2sfp(positions, coords = c("LONGITUDE", "LATITUDE"))

  # Add erroneous fishing operations values for testing the automated consolidation process
  pos$FishingOperation[which(pos$FISHING_TRIP_FK \%in\% "18483452")][97:103] <-  "NotFishing"
  pos$FishingOperation[which(pos$FISHING_TRIP_FK \%in\% "18483452")][6:9] <- "Hauling"

  Nets.list <- Create_NetsByBoat(traj = pos, Use.BehaviourChanges = TRUE)
  set.seed(221107)
  Set_NetThresholds(ls.Nets = Nets.list$Nets)

}

}
\author{
Julien Rodriguez, \email{julien.rodriguez@ifremer.fr}
}
