#' iapesca package data
#'
#' @description
#' A subset of datasets from the French Fisheries Information System (FIS, https://sih.ifremer.fr/),
#' the Ifremer permanent, operational and multidisciplinary national monitoring network for the observation of marine resources and their uses.
#'
#' @name sacrois
#' @format
#' Anonymized consolidated logbooks from SACROIS source.
#' a data.frame with 994 lines and 76 columns, the description being found on:
#' https://sih.ifremer.fr/prive/content/download/141220/file/Sacrois-flux-Utilisateurs-v3.3.11-2112.pdf
#' upon request for external usage.
#'
#' @source <https://sih.ifremer.fr/>
#'
#' @usage data("sacrois")
#'
#' @author Ifremer HISSEO \email{julien.rodriguez@ifremer.fr}
#'
"sacrois"




