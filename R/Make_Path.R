#' Make_Path
#'
#' \code{Make_Path} Create a path architecture from a character string
#'
#' @param path a character string designing the path
#'
#' @return path.expand(path) the link to a valid path, the architecture having been built from the string
#'
#' @author Julien Rodriguez, \email{julien.rodriguez@ifremer.fr}
#'
#' @examples
#'
#'
#' path2create <- sprintf("%s/test/test.1/test.2", tempdir())
#' path <- Make_Path(path2create)
#' dir.exists(path)
#'
#'
#'
#' @export
#'

Make_Path <- function(path){

  if(!dir.exists(path)){

    create.test <- suppressWarnings(inherits(dir.create(path), "try-error"))

    if(!create.test){

      n.folders <- length(unlist(gregexpr(path, pattern = "/")))

      path.architecture <-  c(path, sapply(1:n.folders, function(k){
        parent.k <- dirname(ifelse(k == 1, path, parent.k))
        assign_lsTabs(list(parent.k = parent.k), env = parent.env(environment()))
      }))

      path.build <- sapply(path.architecture[n.folders:1], function(parent.path){
        if(!dir.exists(parent.path)){ dir.create(parent.path) }
      })

    }

  }

  return(path.expand(path))
}

