#' CSquare_Grids
#'
#' \code{CSquare_Grids} Compute relevant CSquare grids as a polygon sf objects and/or as a terra raster from a sf object or an extent
#' If the resolution of CSquares is one of 10, 5, 1, 0.5, 0.1, 0.05, 0.01, the CSquare notations are reported in the objects
#'
#' @param sf.obj a spatial points object of class sf
#' @param extent DEFAULT NULL, a vector, defining spatial extent as xmin, xmax, ymin, ymax
#' @param Csq Resolution of CSquare_Grids, size of the pixels in decimal degrees in WGS84 crs
#' @param class.output DEFAULT "both", defines the output: pol for polygons, ras for raster, both for both
#'
#' @return a list containing two objects Csq.pols for polygons output and Csq.rast for raster output. Defined as NULL depending of class.output values
#'
#' @author Julien Rodriguez, \email{julien.rodriguez@ifremer.fr}
#'
#' @examples
#'
#' require(terra)
#' WorldCSq <- CSquare_Grids(extent = c(-180, 180, -90, 90), Csq = 10)
#' plot(WorldCSq$Csq.rast)
#' plot(sf::st_geometry(WorldCSq$Csq.pols), add = TRUE)
#'
#' @export
#'

CSquare_Grids <- function(sf.obj, extent = NULL, Csq, class.output = "both"){

  if(!is.null(extent)){

    if(!is.numeric(extent)| length(extent) != 4) stop("extent is a numeric defining spatial extent as xmin, xmax, ymin, ymax")

    mat.coords <- expand.grid( X = extent[c(1:2)], Y = extent[c(3:4)])
    sf.obj <- sf::st_sf(coord2sf(as.matrix(mat.coords[c(1,2,4,3), ]), type = "polygon", epsg = 4326))

  }

  if(!inherits(sf.obj, "sf")) stop("sf.obj to be defined as an sf object")
  if(is.na(sf::st_crs(sf.obj))) stop("Missing coordinate reference system not accepted")
  if(!is.numeric(Csq)| length(Csq) > 1 | Csq > 10 | Csq < 0) stop("Csq is a numeric, defined as the size of the pixel Csquare grid in decimal degrees, its value must range between 0 and 10")
  if(!class.output %in% c("both", "pol", "ras") | length(class.output) > 1) stop("class.output must be defined as: both, pol or ras")

  if( sf::st_crs(sf.obj) != sf::st_crs(4326)){
    sf.obj <- sf::st_transform(sf.obj, sf::st_crs(4326))
  }

  obj.bbox <- SfBbox(sf.obj)
  xy.lim <- apply(sf::st_coordinates(obj.bbox), 2, range)[, 1:2]

  Csq.grid <- list(x = seq(-180, 180, by = Csq),
                   y = seq(-90, 90, by = Csq)
  )

  Csq.grid.bb <- Csq.grid
  x.index <- which(Csq.grid$x >  xy.lim[ 1, "X"] & Csq.grid$x <  xy.lim[ 2, "X"])
  if(length(x.index) == 0){
    x.index <- c(max(which(Csq.grid$x <  xy.lim[ 2, "X"])),
                 min(which(Csq.grid$x >  xy.lim[ 1, "X"])))
  }
  x.index <- unique(c(x.index-1, x.index +1))
  y.index <- which(Csq.grid$y >  xy.lim[ 1, "Y"] & Csq.grid$y <  xy.lim[ 2, "Y"])
  if(length(y.index) == 0){
    y.index <- c(max(which(Csq.grid$y <  xy.lim[ 2, "Y"])),
                 min(which(Csq.grid$y >  xy.lim[ 1, "Y"])))
  }
  y.index <- unique(c(y.index-1, y.index +1))

  Csq.grid.bb$x <- Csq.grid.bb$x[ x.index ]
  Csq.grid.bb$y <- Csq.grid.bb$y[ y.index ]

  mat.grid <- data.frame(expand.grid( X = Csq.grid.bb$x, Y = Csq.grid.bb$y))

  if(class.output %in% c("both", "ras")){

    empty.rast <- terra::rast(xmin = min(mat.grid$X),
                              xmax = max(mat.grid$X),
                              ymin = min(mat.grid$Y),
                              ymax = max(mat.grid$Y),
                              crs = terra::crs("epsg:4326"),
                              resolution = Csq
    )

    coord.pixels <- terra::xyFromCell(empty.rast, cell = 1:prod(dim(empty.rast)[1:2]))
    Csq.ids <- try(CSquare(lon = coord.pixels[, 1],
                           lat = coord.pixels[, 2],
                           degrees = Csq), silent = TRUE)
    if(inherits(Csq.ids, "try-error")){ Csq.ids <- rep(NA, prod(dim(empty.rast)[1:2]))}
    Csq.rast <- terra::setValues(empty.rast, data.frame(CSquare = Csq.ids))
    names(Csq.rast) <- "CSquare"

  }else{

    Csq.rast <- NULL

  }

  if(class.output %in% c("both", "pol")){

    seq.x <- nrow(mat.grid)/length(y.index)
    pt.obj <- sf::st_sf(ID = 1:nrow(mat.grid), coord2sf(mat.grid, epsg = 4326))

    index.xpolstart <- unique(unlist(lapply(0:(length(y.index)-2), function(k){
      start.line <- k*seq.x + 1
      index <- start.line:(start.line + seq.x -2)
    })))

    pols <- do.call(rbind, lapply(index.xpolstart, function(k){
      pol.obj <- sf::st_sf( ID = which(index.xpolstart %in% k),
                        geometry = coord2sf(as.matrix(mat.grid[c(k, k+1, seq.x + c(k+1, k)), ]), type = "polygon", epsg = 4326))
      return(pol.obj)
    }))

    centroid.pols <- suppressWarnings(data.frame(sf::st_coordinates(
      sf::st_centroid(pols))))

    Csq.ids <- try(CSquare(lon = centroid.pols$X,
                           lat = centroid.pols$Y,
                           degrees = Csq), silent = TRUE)

    if(inherits(Csq.ids, "try-error")){ Csq.ids <- rep(NA, nrow(pols))}
    pols$CSquare <- Csq.ids
    Csq.pols <- pols[, c("CSquare", "geometry")]

  }else{

    Csq.pols <- NULL

  }

  return(list(Csq.pols = Csq.pols,
              Csq.rast = Csq.rast))
}
